/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*/
//
// Created by ivaughn on 2/2/21.
//

#ifndef DS_MX_STDPRIMITIVE_TRACKLINE3D_H
#define DS_MX_STDPRIMITIVE_TRACKLINE3D_H

#include <ds_mxcore/ds_mxcore.h>
#include <ds_mxcore/ds_mx_action_primitive.h>
#include "speed_control.h"
#include "depth_control.h"
#include "ds_mx_stdprimitives/Trackline3dAction.h"

namespace ds_mx_stdprimitives {

class Trackline3d : public ds_mx::ActionPrimitive<ds_mx_stdprimitives::Trackline3dAction, Trackline3d> {
 public:
  Trackline3d();
  virtual ~Trackline3d();

  void init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) override;
  bool validate() const override;

  void getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay &display) override;

  ds_mx_stdprimitives::Trackline3dGoal toMsg() const;

  void resetTimeout(const ros::Time& now) override;

 protected:
  ds_mx::GeoPointParam start_pt;
  ds_mx::GeoPointParam end_pt;
  ds_mx::DoubleParam start_depth;
  ds_mx::DoubleParam end_depth;
  ds_mx_stdprimitives::SpeedControl speed;

  // nominal heading.  Defaults to NaN
  ds_mx::DoubleParam heading;

  // Force the heading to the given value, regardless of any other considerations
  ds_mx::BoolParam force_heading;

  // Specify a transition region for setting heading.  If set to 0, a default value for the node is used
  ds_mx::DoubleParam heading_radius;
  ds_mx::DoubleParam heading_radius_width;

  // bool to specify that the heading radius should be specified in the horizontal plane, rather than in the plane
  // normal to the desired direction of travel
  ds_mx::BoolParam heading_radius_horizontal;

  ds_mx::DoubleParam timeout_multiplier;
  ds_mx::DoubleParam timeout_increment;
  ds_mx::DoubleParam timeout_assumed_speed;

  double getPredictedSpeed() const;
};

} // namespace ds_mx_stdprimitives

#endif // DS_MX_STDPRIMITIVE_TRACKLINE3D_H

