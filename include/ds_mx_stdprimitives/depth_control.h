/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*/
//
// Created by ivaughn on 6/16/20.
//

#ifndef DS_MX_STDPRIMITIVES_DEPTH_CONTROL_H
#define DS_MX_STDPRIMITIVES_DEPTH_CONTROL_H

#include <ds_mxcore/ds_mxcore.h>
#include <ds_mx_stdprimitives/Depth.h>

namespace ds_mx_stdprimitives {

/// \brief Convienence class to wrap all depth control stuff
class DepthControl {
 public:
  DepthControl(std::shared_ptr<ds_mx::ParameterCollection>& col, const std::string& name);

  bool validate() const;

  ds_mx_stdprimitives::Depth toMsg() const;

 protected:
  ds_mx::IntParam depth_mode;

  ds_mx::DoubleParam depth_goal;
  ds_mx::DoubleParam depth_floor;
  ds_mx::DoubleParam depth_ceiling;

  ds_mx::DoubleParam speedlimit;

  ds_mx::DoubleParam min_altitude;
  ds_mx::DoubleParam altitude;

  ds_mx::DoubleParam triangle_rate;
};

} // namespace ds_mx_stdprimitives

#endif // DS_MX_STDPRIMITIVES_DEPTH_CONTROL_H
