# Standard Primitives

This package implements a set of standard actionlib-based primitives.  The idea is to make it easy to adapt MX to new 
platforms by simply implementing a subset of the available primitives.

# Timeouts

Timeouts are kind of complicated, as there are multiple places to implement them.

In general, however, the rules are implemented as follows.  ALL mx tasks include a ```max_timeout``` parameter.  This
parameter will remain the maximum timeout, and remains implemented at the MX level.  In addition, standard primitives
include an ```duration``` parameter.  This is included in the action server message, and is implemented by the 
ActionServer that implements the interface.  The action_timeout should always be shorter than the ```max_timeout```.  

A task is considered to have "failed" if the ```max_timeout``` halts it.  It may succeed or fail if the 
```duration``` halts it; this behavior is primitive-dependent.  

Finally, the trackline task has the ability should generally be permitted to compute its own ```duration```
based on the length of the trackline and the requested speed.

# Tasks
## Idle

* Task Name: ```stdprim_idle```
* Returns: SUCCESS if ```duration``` is reached, FAILED if ```max_timeout``` is reached
* Timeout: If unspecified, ```max_timeout``` is set to ```duration``` + 5 seconds
* Description: The Idle Task commands the vehicle to sit idle, without any actuators spinning.  It works by enabling the 
Idle allocation.  While the behavior of the Idle allocation is vehicle-specific, it generally commands thrusters
to 0 speed and maintains the current angle for all servos.

## Joystick

* Task Name: ```stdprim_joystick```
* Returns: SUCCESS if ```duration``` is reached, FAILED if ```max_timeout``` is reached
* Timeout: If unspecified, ```max_timeout``` is set to ```duration``` + 5 seconds
* Description: The Joystick Task allows a user to specify joystick-style commands in a mission file.  This is most
commonly used for open-loop control-- for example, "thrust up for 5 seconds."

| Parameter | Type | Description |
|:----------|:---------|:-----------|
| ```auto_xy``` | Bool | Enable AutoXY mode |
| ```fwd``` | Double | Forward joystick value.  Units may vary |
| ```stbd``` | Double | Starboard joystick value.  Units may vary |
| ```auto_depth``` | Bool | Enable AutoDepth mode |
| ```down``` | Double | Down joystick value.  Units may vary |
| ```auto_heading``` | Bool | Enable AutoHeading mode |
| ```heading``` | Double | Heading (rate) joystick value.  Units may vary |
| ```auto_rollpitch``` | Bool | Enable AutoRollPitch mode.  Not implemented on most vehicles |
| ```roll``` | Double | Roll joystick value.  Units may vary |
| ```pitch``` | Double | Pitch joystick value.  Units may vary |

## Loiter

* Task Name: ```stdprim_loiter```
* Returns: SUCCESS if ```duration``` is reached, FAILED if ```max_timeout``` is reached
* Timeout: If unspecified, ```max_timeout``` is set to ```duration``` + 5 seconds
* Description: The Loiter Task directs a vehicle to stay near a specific location for a given duration.  If the vehicle
is not yet at this location, it shall drive there before loitering.  There can be a fair bit of difference betweeen
vehicles in how exactly this works.  A hover-capable vehicle, for example, might hover on the goal location.  A 
torpedo-shaped vehicle may instead circle the goal position.


## Loiter Surface

## Manual

## Trackline