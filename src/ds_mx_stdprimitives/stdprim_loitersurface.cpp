

/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*/
//
// Created by ivaughn on 6/16/20.
//

#include "ds_mx_stdprimitives/stdprim_loitersurface.h"

#include <pluginlib/class_list_macros.h>
#include <ds_libtrackline/WktUtil.h>

namespace ds_mx_stdprimitives {

LoiterSurface::LoiterSurface() : ActionDurationPrimitive<ds_mx_stdprimitives::LoiterSurfaceAction, LoiterSurface>("action_loitersurface"),
                   loiter_point(params, "point", ds_mx::ParameterFlag::DYNAMIC),
                   any_heading(params, "any_heading", false, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
                   heading(params, "heading", std::numeric_limits<double>::quiet_NaN(), ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
                   radius(params, "radius", ds_mx::ParameterFlag::DYNAMIC),
                   radius_width(params, "radius_width", ds_mx::ParameterFlag::DYNAMIC),
                   fail_radius(params, "fail_radius", -1, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
                   speed(params, "speed") {
  // do nothing else
}

LoiterSurface::~LoiterSurface() = default;

void LoiterSurface::init(const Json::Value &config, ds_mx::MxCompilerPtr compiler) {
  ActionDurationPrimitive<ds_mx_stdprimitives::LoiterSurfaceAction, LoiterSurface>::init(config, compiler);
}

bool LoiterSurface::validate() const {
  bool ret = durationValid() & speed.validate();

  if (radius.get() < 0) {
    ROS_ERROR_STREAM("Invalid watch circle specified!");
    ret = false;
  }

  return ret;
}

void LoiterSurface::getDisplay(ds_nav_msgs::NavState &state, ds_mx_msgs::MissionDisplay &display) {
  state.header.stamp = getSimEnd(state);

  ds_mx::GeoPoint pt = loiter_point.get();
  state.lon = pt.x;
  state.lat = pt.y;

  ds_mx_msgs::MissionElementDisplay element;
  element.role = ds_mx_msgs::MissionElementDisplay::ROLE_LOITER;
  element.wellknowntext = ds_trackline::wktPointLL(state.lon, state.lat);
  std::copy(uuid.begin(), uuid.end(), element.task_uuid.begin());
  display.elements.push_back(element);
}

ds_mx_stdprimitives::LoiterSurfaceGoal LoiterSurface::toMsg() const {
  ds_mx_stdprimitives::LoiterSurfaceGoal ret;

  ret.finish_at = getEndTime();

  ds_mx::GeoPoint pt = loiter_point.get();
  ret.longitude = pt.x;
  ret.latitude = pt.y;
  ret.any_heading = any_heading.get();
  ret.heading = heading.get();
  ret.radius = radius.get();
  ret.radius_width = radius.get();
  ret.fail_radius = fail_radius.get();
  ret.speed_control = speed.toMsg();

  return ret;
}

} // namespace ds_mx_stdprimitives

PLUGINLIB_EXPORT_CLASS(ds_mx_stdprimitives::LoiterSurface, ds_mx::MxTask)
