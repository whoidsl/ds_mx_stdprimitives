/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*/
//
// Created by ivaughn on 6/16/20.
//

#include "ds_mx_stdprimitives/stdprim_idle.h"

#include <pluginlib/class_list_macros.hpp>
#include <ds_libtrackline/WktUtil.h>

namespace ds_mx_stdprimitives {


Idle::Idle() : ActionDurationPrimitive<ds_mx_stdprimitives::IdleAction, Idle>("action_idle")
    { /* do nothing */ }
Idle::~Idle() = default;

void Idle::init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) {
  ActionDurationPrimitive<ds_mx_stdprimitives::IdleAction, Idle>::init(config, compiler);
}

bool Idle::validate() const {
  return durationValid();
}

void Idle::getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay &display) {
  state.header.stamp = getSimEnd(state);

  ds_mx_msgs::MissionElementDisplay element;
  element.role = ds_mx_msgs::MissionElementDisplay::ROLE_IDLE;
  element.wellknowntext = ds_trackline::wktPointLL(state.lon, state.lat);
  std::copy(uuid.begin(), uuid.end(), element.task_uuid.begin());
  display.elements.push_back(element);
}

ds_mx_stdprimitives::IdleGoal Idle::toMsg() const {
  ds_mx_stdprimitives::IdleGoal ret;

  ret.finish_at = getEndTime();

  return ret;
}

} // namespace ds_mx_stdprimitives

PLUGINLIB_EXPORT_CLASS(ds_mx_stdprimitives::Idle, ds_mx::MxTask)