/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*/
//
// Created by ivaughn on 6/16/20.
//

#include "ds_mx_stdprimitives/stdprim_manual.h"

#include <pluginlib/class_list_macros.h>
#include <ds_libtrackline/WktUtil.h>

namespace ds_mx_stdprimitives {

Manual::Manual() : ActionDurationPrimitive<ds_mx_stdprimitives::ManualAction, Manual>("action_manual"),
                   heading(params, "heading", ds_mx::ParameterFlag::DYNAMIC),
                   heading_rate(params, "heading_rate", 0, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
                   speed(params, "speed"),
                   depth(params, "depth")
{ /* do nothing */ }

Manual::~Manual() = default;

void Manual::init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) {
  ActionDurationPrimitive<ds_mx_stdprimitives::ManualAction, Manual>::init(config, compiler);
}

bool Manual::validate() const {
  bool ret = durationValid() & speed.validate() & depth.validate();

  if (heading.get() < 0.0 || heading.get() > 360.0) {
    ROS_ERROR_STREAM("Manual primitive " <<name <<" must have heading between 0 and 360");
    ret = false;
  }

  return ret;
}

void Manual::getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay &display) {
  state.header.stamp = getSimEnd(state);

  // TODO It'd be awfully nice to draw something less useless

  ds_mx_msgs::MissionElementDisplay element;
  element.role = ds_mx_msgs::MissionElementDisplay::ROLE_MANUAL;
  element.wellknowntext = ds_trackline::wktPointLL(state.lon, state.lat);
  std::copy(uuid.begin(), uuid.end(), element.task_uuid.begin());
  display.elements.push_back(element);
}

ds_mx_stdprimitives::ManualGoal Manual::toMsg() const {
  ds_mx_stdprimitives::ManualGoal ret;

  ret.finish_at = getEndTime();

  ret.heading = heading.get();
  ret.heading_rate = heading_rate.get();

  ret.speed_control = speed.toMsg();
  ret.depth_control = depth.toMsg();

  return ret;
}

} // namespace ds_mx_stdprimitives

PLUGINLIB_EXPORT_CLASS(ds_mx_stdprimitives::Manual, ds_mx::MxTask)