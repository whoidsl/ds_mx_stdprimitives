/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*/
//
// Created by ivaughn on 6/16/20.
//

#include "ds_mx_stdprimitives/speed_control.h"

namespace ds_mx_stdprimitives {

SpeedControl::SpeedControl(std::shared_ptr<ds_mx::ParameterCollection>& col, const std::string& name) :
    speed_mode(col, name + "_mode", ds_mx::ParameterFlag::DYNAMIC),
    speed_value(col, name + "_value", ds_mx::ParameterFlag::DYNAMIC) {
  // do nothing else
}

bool SpeedControl::validate() const {
  bool ret = true;
  switch(speed_mode.get()) {
    case ds_mx_stdprimitives::Speed::SPEEDMODE_OPENLOOP:
    case ds_mx_stdprimitives::Speed::SPEEDMODE_CLOSEDLOOP:
      // don't really need to validate anything
      break;

    default:
      ROS_ERROR_STREAM("SpeedControl does not specify a valid mode!");
      ret = false;
  }

  return ret;
}

ds_mx_stdprimitives::Speed SpeedControl::toMsg() const {
  ds_mx_stdprimitives::Speed ret;

  ret.mode = speed_mode.get();
  ret.value = speed_value.get();

  return ret;
}

int SpeedControl::mode() const {
  return speed_mode.get();
}

double SpeedControl::value() const {
  return speed_value.get();
}

bool SpeedControl::isOpenLoop() const {
  return speed_mode.get() == ds_mx_stdprimitives::Speed::SPEEDMODE_OPENLOOP;
}

bool SpeedControl::isClosedLoop() const {
  return speed_mode.get() == ds_mx_stdprimitives::Speed::SPEEDMODE_CLOSEDLOOP;
}

} // namespace ds_mx_stdprimitives
