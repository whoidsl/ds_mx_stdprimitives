/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*/
//
// Created by ivaughn on 6/16/20.
//

#include "ds_mx_stdprimitives/depth_control.h"

namespace ds_mx_stdprimitives {

DepthControl::DepthControl(std::shared_ptr<ds_mx::ParameterCollection>& col, const std::string& name) :
depth_mode(col, name+"_mode", -1, ds_mx::ParameterFlag::DYNAMIC),
depth_goal(col, name+"_goal", std::numeric_limits<double>::quiet_NaN(), ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
depth_floor(col, name+"_floor", std::numeric_limits<double>::quiet_NaN(), ds_mx::ParameterFlag::DYNAMIC),
depth_ceiling(col, name+"_ceiling", std::numeric_limits<double>::quiet_NaN(), ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
speedlimit(col, name+"_speedlimit", std::numeric_limits<double>::quiet_NaN(), ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
min_altitude(col, name+"_min_altitude", std::numeric_limits<double>::quiet_NaN(), ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
altitude(col, name+"_altitude", std::numeric_limits<double>::quiet_NaN(), ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
triangle_rate(col, name+"_triangle_rate", std::numeric_limits<double>::quiet_NaN(), ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL)
{
  // do nothing else
}

bool DepthControl::validate() const {
  bool ret = true;
  if (std::isnan(depth_floor.get()) || depth_floor.get() < 0) {
    ROS_ERROR_STREAM("DepthControl depth floor is not specified!");
    ret = false;
  }

  switch(depth_mode.get()) {
    case ds_mx_stdprimitives::Depth::DEPTHMODE_SURFACE:
      // really nothing to see here
      break;
    case ds_mx_stdprimitives::Depth::DEPTHMODE_DEPTH:
      if (std::isnan(depth_goal.get()) || depth_goal.get() < 0) {
        ROS_ERROR_STREAM("DepthControl in DEPTH mode, but no goal specified!");
        ret = false;
      }
      if (!std::isnan(speedlimit.get()) && speedlimit.get() <= 0) {
        ROS_ERROR_STREAM("DepthControl specifies a negative or zero max speed!");
        ret = false;
      }
      break;
    case ds_mx_stdprimitives::Depth::DEPTHMODE_ALTITUDE:
      if (std::isnan(altitude.get()) || altitude.get() <= 0) {
        ROS_ERROR_STREAM("DepthControl in ALTITUDE mode, but no altitude goal specified!");
        ret = false;
      }
      if (!std::isnan(speedlimit.get()) && speedlimit.get() <= 0) {
        ROS_ERROR_STREAM("DepthControl specifies a negative or zero max speed!");
        ret = false;
      }
      break;
    case ds_mx_stdprimitives::Depth::DEPTHMODE_TRIANGLE:
    {
      if (std::isnan(min_altitude.get()) || min_altitude.get() <= 0) {
        ROS_ERROR_STREAM("DepthControl in ALTITUDE mode, but no min_altitude specified!");
        ret = false;
      }
      if (std::isnan(depth_ceiling.get()) || depth_ceiling.get() < 0) {
        ROS_ERROR_STREAM("DepthControl in TRIANGLE mode, but no depth_ceiling specified!");
        ret = false;
      }
      if (std::isnan(triangle_rate.get())) {
        ROS_ERROR_STREAM("DepthControl in ALTITUDE mode, but no triangle_rate specified!");
        ret = false;
      }
    }
      break;
    default:
      ROS_ERROR_STREAM("Unknown depth mode!");
      ret = false;
  }

  return ret;
}

ds_mx_stdprimitives::Depth DepthControl::toMsg() const {
  ds_mx_stdprimitives::Depth ret;

  ret.depth_mode = depth_mode.get();

  ret.depth = depth_goal.get();
  ret.depth_ceiling = depth_ceiling.get();
  ret.depth_floor = depth_floor.get();
  ret.depth_speedlimit = speedlimit.get();

  ret.min_altitude = min_altitude.get();
  ret.altitude = altitude.get();

  ret.triangle_rate = triangle_rate.get();

  return ret;
}

} // namespace ds_mx_stdprimitives
