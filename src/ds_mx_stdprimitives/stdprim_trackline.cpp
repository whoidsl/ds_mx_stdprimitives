/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*/
//
// Created by ivaughn on 6/16/20.
//

#include "ds_mx_stdprimitives/stdprim_trackline.h"

#include <pluginlib/class_list_macros.h>
#include <ds_libtrackline/Trackline.h>
#include <ds_libtrackline/WktUtil.h>

namespace ds_mx_stdprimitives {

Trackline::Trackline() : ActionPrimitive<ds_mx_stdprimitives::TracklineAction, Trackline> ("action_trackline"),
  start_pt(params, "start_pt", ds_mx::ParameterFlag::DYNAMIC),
  end_pt(params, "end_pt", ds_mx::ParameterFlag::DYNAMIC),
  speed(params, "speed"),
  depth(params, "depth"),
  timeout_multiplier(params, "timeout_multiplier", 2.0, ds_mx::ParameterFlag::STATIC | ds_mx::ParameterFlag::OPTIONAL),
  timeout_increment(params, "timeout_increment", 60.0, ds_mx::ParameterFlag::STATIC | ds_mx::ParameterFlag::OPTIONAL),
  timeout_assumed_speed(params, "timeout_assumed_speed", 1.0, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL)
{ /* do nothing */ }

Trackline::~Trackline() = default;

void Trackline::init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) {
  ActionPrimitive<ds_mx_stdprimitives::TracklineAction, Trackline>::init(config, compiler);
}

bool Trackline::validate() const {
  bool ret = speed.validate() & depth.validate();

  return ret;
}

void Trackline::getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay &display) {

  // extarct start/end & build a trackline for length stuff
  const auto start = start_pt.get();
  const auto end = end_pt.get();
  ds_trackline::Trackline trackline1(state.lon, state.lat, start.x, start.y);
  ds_trackline::Trackline trackline2(start.x, start.y, end.x, end.y);

  ds_mx_msgs::MissionElementDisplay line1;
  line1.role = ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE_CONNECTING;
  line1.wellknowntext = trackline1.getWktLL();
  std::copy(uuid.begin(), uuid.end(), line1.task_uuid.begin());
  display.elements.push_back(line1);

  ds_mx_msgs::MissionElementDisplay line2;
  line2.role = ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE;
  line2.wellknowntext = trackline2.getWktLL();
  std::copy(uuid.begin(), uuid.end(), line2.task_uuid.begin());
  display.elements.push_back(line2);

  // update state to nominal location
  state.lon = end.x;
  state.lat = end.y;

  // account for time
  double predicted_speed = getPredictedSpeed();
  // dividing by zero often offends; ditto for going backwards in time
  if (predicted_speed > 0.001) {
    state.header.stamp += ros::Duration(trackline1.getLength() / predicted_speed);
    state.header.stamp += ros::Duration(trackline2.getLength() / predicted_speed);
  }
}

void Trackline::resetTimeout(const ros::Time &now) {
  double expected_time = 0;
  double expected_speed = 0;
  expected_speed = getPredictedSpeed();
  if (expected_speed > 0.001) {
    // don't divide by zero / negative speed
    auto start = start_pt.get();
    auto end = end_pt.get();
    ds_trackline::Trackline trackline(start.x, start.y, end.x, end.y);
    expected_time = trackline.getLength() / expected_speed;
  }

  timeout = now + ros::Duration(expected_time * timeout_multiplier.get() + timeout_increment.get());
}

double Trackline::getPredictedSpeed() const {
  double predicted_speed = 0.0;
  if (speed.isClosedLoop()) {
    predicted_speed = speed.value();
  } else {
    if (!speed.isOpenLoop()) {
      ROS_WARN_STREAM("stdprim_trackline: Unknown speed mode, falling back on assumed speed of " <<timeout_assumed_speed.get());
    }
    predicted_speed = timeout_assumed_speed.get();
  }

  return predicted_speed;
}

ds_mx_stdprimitives::TracklineGoal Trackline::toMsg() const {
  ds_mx_stdprimitives::TracklineGoal ret;

  ds_mx::GeoPoint start_pt_ = start_pt.get();
  ret.longitude_start = start_pt_.x;
  ret.latitude_start = start_pt_.y;

  ds_mx::GeoPoint end_pt_ = end_pt.get();
  ret.longitude_end = end_pt_.x;
  ret.latitude_end = end_pt_.y;

  ret.speed_control = speed.toMsg();
  ret.depth_control = depth.toMsg();

  return ret;
}

} // namespace ds_mx_stdprimitives

PLUGINLIB_EXPORT_CLASS(ds_mx_stdprimitives::Trackline, ds_mx::MxTask)
