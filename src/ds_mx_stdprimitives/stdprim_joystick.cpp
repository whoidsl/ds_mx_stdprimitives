/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*/
//
// Created by ivaughn on 6/16/20.
//

#include "ds_mx_stdprimitives/stdprim_joystick.h"

#include <pluginlib/class_list_macros.h>
#include <ds_libtrackline/WktUtil.h>

namespace ds_mx_stdprimitives {

Joystick::Joystick() : ActionDurationPrimitive<ds_mx_stdprimitives::JoystickAction, Joystick>("action_joystick"),
  value_fwd(params, "fwd", 0, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
  value_stbd(params, "stbd", 0, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
  value_down(params, "down", 0, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
  value_heading(params, "heading", 0, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
  value_pitch(params, "pitch", 0, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
  value_roll(params, "roll", 0, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
  auto_xy(params, "auto_xy", false, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
  auto_heading(params, "auto_heading", false, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
  auto_depth(params, "auto_depth", false, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
  auto_rollpitch(params, "auto_rollpitch", false, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL)
{ /* do nothing */ }
Joystick::~Joystick() = default;

void Joystick::init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) {
  ActionDurationPrimitive<ds_mx_stdprimitives::JoystickAction, Joystick>::init(config, compiler);
}

bool Joystick::validate() const {
  return durationValid();
}

void Joystick::getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay &display) {
  state.header.stamp = getSimEnd(state);

  ds_mx_msgs::MissionElementDisplay element;
  element.role = ds_mx_msgs::MissionElementDisplay::ROLE_JOYSTICK;
  element.wellknowntext = ds_trackline::wktPointLL(state.lon, state.lat);
  std::copy(uuid.begin(), uuid.end(), element.task_uuid.begin());
  display.elements.push_back(element);
}

ds_mx_stdprimitives::JoystickGoal Joystick::toMsg() const {
  ds_mx_stdprimitives::JoystickGoal ret;

  ret.finish_at = getEndTime();

  ret.auto_xy = auto_xy.get();
  ret.fwd = value_fwd.get();
  ret.stbd = value_stbd.get();

  ret.auto_depth = auto_depth.get();
  ret.down = value_down.get();

  ret.auto_heading = auto_heading.get();
  ret.heading = value_heading.get();

  ret.auto_rollpitch = auto_rollpitch.get();
  ret.roll = value_roll.get();
  ret.pitch = value_pitch.get();

  return ret;
}

} // namespace ds_mx_stdprimitives

PLUGINLIB_EXPORT_CLASS(ds_mx_stdprimitives::Joystick, ds_mx::MxTask)