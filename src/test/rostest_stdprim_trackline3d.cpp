/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*/
//
// Created by ivaughn on 2/2/21.
//

#include <gtest/gtest.h>

#include <ds_mxcore/test/ds_mock_action_server.h>

// we'll build this test using Idle actions as an example; however, the point of the test is NOT
// to test the Idle test-- it's to test the underlying Action primitive
#include <ds_mx_stdprimitives/stdprim_trackline3d.h>

class StdPrimTrackline3dRostest : public ds_mx::MockActionServer<ds_mx_stdprimitives::Trackline3dAction, ds_mx_stdprimitives::Trackline3d> {
 protected:
  virtual std::string getJSON() const {
    return R"( {
  "type": "stdprim_trackline3d",
  "start_pt": "POINT(2.0001 1.0001) ",
  "start_depth": 10,
  "end_pt": "POINT(2.0001 1.0002) ",
  "end_depth": 11,
  "speed_mode": 1,
  "speed_value": 0.75,
})";
  }
};

TEST_F(StdPrimTrackline3dRostest, TestValidate) {
  EXPECT_TRUE(underTest->validate());
}

TEST_F(StdPrimTrackline3dRostest, TestInvalidSpeed) {
  // we do a simple test to make sure the speed check is getting called;
  // detailed evaluation of failure modes is tested elsewhere
  underTest->getParameters().get<ds_mx::IntParam>("speed_mode").set(-1);
  EXPECT_FALSE(underTest->validate());
}

TEST_F(StdPrimTrackline3dRostest, TestInvalidStartDepth) {
  underTest->getParameters().get<ds_mx::DoubleParam>("start_depth").set(-0.001);
  EXPECT_FALSE(underTest->validate());
}

TEST_F(StdPrimTrackline3dRostest, TestInvalidEndDepth) {
  underTest->getParameters().get<ds_mx::DoubleParam>("end_depth").set(-0.001);
  EXPECT_FALSE(underTest->validate());
}

TEST_F(StdPrimTrackline3dRostest, TestInvalidHeadingRadius) {
  underTest->getParameters().get<ds_mx::DoubleParam>("heading_radius").set(-0.001);
  EXPECT_FALSE(underTest->validate());
}

TEST_F(StdPrimTrackline3dRostest, TestInvalidHeadingRadiusWidth) {
  underTest->getParameters().get<ds_mx::DoubleParam>("heading_radius_width").set(-0.001);
  EXPECT_FALSE(underTest->validate());
}

TEST_F(StdPrimTrackline3dRostest, TestDisplay) {
  ds_mx_msgs::MissionDisplay display;

  state.lon = 2.0;
  state.lat = 1.0;
  underTest->getDisplay(state, display);

  ASSERT_EQ(2, display.elements.size());

  EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE_CONNECTING, display.elements[0].role);
  EXPECT_EQ("LINESTRING (2.000000000 1.000000000, 2.000100000 1.000100000)", display.elements[0].wellknowntext);

  EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE, display.elements[1].role);
  EXPECT_EQ("LINESTRING (2.000100000 1.000100000, 2.000100000 1.000200000)", display.elements[1].wellknowntext);
}

TEST_F(StdPrimTrackline3dRostest, TestSuccess) {
  state.header.stamp = ros::Time::now();

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
    ASSERT_TRUE(static_cast<bool>(last_goal));
    ros::Time timeoutTime = underTest->timeoutTime();
    ros::Duration timeoutToGo = timeoutTime - state.header.stamp;
    EXPECT_NEAR( 89.6069, timeoutToGo.toSec(), 0.001);
  }

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "rostest_stdprim_trackline");
  ros::NodeHandle nh;

  ros::AsyncSpinner spinner(2); // use two threads
  spinner.start(); // returns immediately

  ds_mx::eventlog_disable();

  return RUN_ALL_TESTS();
}
