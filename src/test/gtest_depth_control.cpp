/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/2/20.
//

#include <gtest/gtest.h>

#include <ds_mxcore/test/ds_mock_compiler.h>

#include <ds_mx_stdprimitives/depth_control.h>

class DepthControlTestTask : public ds_mx::MxPrimitiveTask {
  public:
  DepthControlTestTask() : depth_control(params, "depth") { /* do nothing */ };

  // just make public, because test
  ds_mx_stdprimitives::DepthControl depth_control;

  // these methods must be implemented because they're pure virtual
  bool validate() const override { return depth_control.validate(); }
  void getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay& display) override { /* do nothing */}
  void init_ros(ros::NodeHandle& nh) override { /* do nothing */ }

 protected:
  ds_mx::TaskReturnCode onTick(const ds_nav_msgs::NavState& state) override {
   // doesn't really matter what we do here
   return ds_mx::TaskReturnCode::RUNNING;
 }
};

class DepthControlTest : public ::testing::Test {
  void SetUp() override {
    /* do nothing */
  }

 public:
  const ds_mx_stdprimitives::DepthControl& buildFromString(const std::string& str) {
    // parse the JSON
    Json::Value config;
    Json::Reader reader;
    reader.parse(str, config);

    std::shared_ptr<ds_mx::MxMockCompiler> compiler = ds_mx::MxMockCompiler::create();
    test_task.init(config, compiler);

    return test_task.depth_control;
  }

  DepthControlTestTask test_task;


};

TEST_F(DepthControlTest, parse_surface) {
  auto underTest = buildFromString( R"( {
  "depth_mode": 0,
  "depth_floor": 100.0
})");

  auto msg = underTest.toMsg();
  EXPECT_EQ(ds_mx_stdprimitives::Depth::DEPTHMODE_SURFACE, msg.depth_mode);
  EXPECT_TRUE(std::isnan(msg.depth));
  EXPECT_DOUBLE_EQ(100.0, msg.depth_floor);
  EXPECT_TRUE(std::isnan(msg.depth_ceiling));
  EXPECT_TRUE(std::isnan(msg.depth_speedlimit));
  EXPECT_TRUE(std::isnan(msg.min_altitude));
  EXPECT_TRUE(std::isnan(msg.altitude));
  EXPECT_TRUE(std::isnan(msg.triangle_rate));
}

TEST_F(DepthControlTest, parse_depth) {
  auto underTest = buildFromString( R"( {
  "depth_mode": 1,
  "depth_goal": 10.0,
  "depth_speedlimit": 1.0,
  "depth_floor": 100.0
})");

  auto msg = underTest.toMsg();
  EXPECT_EQ(ds_mx_stdprimitives::Depth::DEPTHMODE_DEPTH, msg.depth_mode);
  EXPECT_DOUBLE_EQ(10.0, msg.depth);
  EXPECT_DOUBLE_EQ(msg.depth_floor, 100.0);
  EXPECT_TRUE(std::isnan(msg.depth_ceiling));
  EXPECT_DOUBLE_EQ(msg.depth_speedlimit, 1.0);
  EXPECT_TRUE(std::isnan(msg.min_altitude));
  EXPECT_TRUE(std::isnan(msg.altitude));
  EXPECT_TRUE(std::isnan(msg.triangle_rate));
}

TEST_F(DepthControlTest, parse_altitude) {
  auto underTest = buildFromString( R"( {
  "depth_mode": 2,
  "depth_altitude": 10.0,
  "depth_speedlimit": 1.0,
  "depth_floor": 100.0
})");

  auto msg = underTest.toMsg();
  EXPECT_EQ(ds_mx_stdprimitives::Depth::DEPTHMODE_ALTITUDE, msg.depth_mode);
  EXPECT_TRUE(std::isnan(msg.depth));
  EXPECT_DOUBLE_EQ(100.0, msg.depth_floor);
  EXPECT_TRUE(std::isnan(msg.depth_ceiling));
  EXPECT_DOUBLE_EQ(1.0, msg.depth_speedlimit);
  EXPECT_TRUE(std::isnan(msg.min_altitude));
  EXPECT_DOUBLE_EQ(10.0, msg.altitude);
  EXPECT_TRUE(std::isnan(msg.triangle_rate));
}

TEST_F(DepthControlTest, parse_triangle) {
  auto underTest = buildFromString( R"( {
  "depth_mode": 3,
  "depth_ceiling": 10.0,
  "depth_floor": 100.0,
  "depth_min_altitude": 3.0,
  "depth_triangle_rate": 1.5
})");

  auto msg = underTest.toMsg();
  EXPECT_EQ(ds_mx_stdprimitives::Depth::DEPTHMODE_TRIANGLE, msg.depth_mode);
  EXPECT_TRUE(std::isnan(msg.depth));
  EXPECT_DOUBLE_EQ(100.0, msg.depth_floor);
  EXPECT_DOUBLE_EQ(10.0, msg.depth_ceiling);
  EXPECT_TRUE(std::isnan(msg.depth_speedlimit));
  EXPECT_DOUBLE_EQ(3.0, msg.min_altitude);
  EXPECT_TRUE(std::isnan(msg.altitude));
  EXPECT_DOUBLE_EQ(1.5, msg.triangle_rate);
}

TEST_F(DepthControlTest, valid_surface) {
  auto underTest = buildFromString( R"( {
  "depth_mode": 0,
  "depth_floor": 100.0
})");

  EXPECT_TRUE(underTest.validate());
}

TEST_F(DepthControlTest, valid_depth) {
  auto underTest = buildFromString( R"( {
  "depth_mode": 1,
  "depth_goal": 1.0,
  "depth_floor": 100.0
})");

  EXPECT_TRUE(underTest.validate());
}

TEST_F(DepthControlTest, invalid_depth_neg) {
  auto underTest = buildFromString( R"( {
  "depth_mode": 1,
  "depth_goal": -1.0,
  "depth_floor": 100.0
})");

  EXPECT_FALSE(underTest.validate());
}

TEST_F(DepthControlTest, invalid_depth_not_set) {
  auto underTest = buildFromString( R"( {
  "depth_mode": 1,
  "depth_floor": 100.0
})");

  EXPECT_FALSE(underTest.validate());
}

TEST_F(DepthControlTest, invalid_depth_negfloor) {
  auto underTest = buildFromString( R"( {
  "depth_mode": 1,
  "depth_goal": 1.0,
  "depth_floor": -0.1
})");

  EXPECT_FALSE(underTest.validate());
}

TEST_F(DepthControlTest, invalid_depth_zerorate) {
  auto underTest = buildFromString( R"( {
  "depth_mode": 1,
  "depth_goal": 1.0,
  "depth_floor": 100.0,
  "depth_speedlimit": 0
})");

  EXPECT_FALSE(underTest.validate());
}

TEST_F(DepthControlTest, invalid_depth_negrate) {
  auto underTest = buildFromString( R"( {
  "depth_mode": 1,
  "depth_goal": 1.0,
  "depth_floor": 100.0,
  "depth_speedlimit": -0.001
})");

  EXPECT_FALSE(underTest.validate());
}

TEST_F(DepthControlTest, valid_depth_validrate) {
  auto underTest = buildFromString( R"( {
  "depth_mode": 1,
  "depth_goal": 1.0,
  "depth_floor": 100.0,
  "depth_speedlimit": 0.001
})");

  EXPECT_TRUE(underTest.validate());
}

TEST_F(DepthControlTest, valid_alt) {
  auto underTest = buildFromString(R"( {
  "depth_mode": 2,
  "depth_altitude": 1.0,
  "depth_floor": 100.0
})");

  EXPECT_TRUE(underTest.validate());
}

TEST_F(DepthControlTest, valid_alt_speedlimit) {
  auto underTest = buildFromString(R"( {
  "depth_mode": 2,
  "depth_altitude": 1.0,
  "depth_floor": 100.0,
  "depth_speedlimit": 0.001
})");

  EXPECT_TRUE(underTest.validate());
}

TEST_F(DepthControlTest, invalid_alt_zero) {
  auto underTest = buildFromString(R"( {
  "depth_mode": 2,
  "depth_altitude": 0.0,
  "depth_floor": 100.0
})");

  EXPECT_FALSE(underTest.validate());
}

TEST_F(DepthControlTest, invalid_alt_neg) {
  auto underTest = buildFromString(R"( {
  "depth_mode": 2,
  "depth_altitude": -0.1,
  "depth_floor": 100.0
})");

  EXPECT_FALSE(underTest.validate());
}

TEST_F(DepthControlTest, invalid_alt_not_set) {
  auto underTest = buildFromString(R"( {
  "depth_mode": 2,
  "depth_floor": 100.0
})");

  EXPECT_FALSE(underTest.validate());
}

TEST_F(DepthControlTest, invalid_alt_negfloor) {
  auto underTest = buildFromString(R"( {
  "depth_mode": 2,
  "depth_altitude": 1.0,
  "depth_floor": -100.0
})");

  EXPECT_FALSE(underTest.validate());
}

TEST_F(DepthControlTest, invalid_alt_zerospeedlimit) {
  auto underTest = buildFromString(R"( {
  "depth_mode": 2,
  "depth_altitude": 1.0,
  "depth_floor": 100.0,
  "depth_speedlimit": 0
})");

  EXPECT_FALSE(underTest.validate());
}

TEST_F(DepthControlTest, invalid_alt_negspeedlimit) {
  auto underTest = buildFromString(R"( {
  "depth_mode": 2,
  "depth_altitude": 1.0,
  "depth_floor": 100.0,
  "depth_speedlimit": -0.001
})");

  EXPECT_FALSE(underTest.validate());
}

TEST_F(DepthControlTest, valid_triag) {
  auto underTest = buildFromString(R"( {
  "depth_mode": 3,
  "depth_min_altitude": 1.0,
  "depth_ceiling": 1.0,
  "depth_triangle_rate": 1.0,
  "depth_floor": 100.0
})");

  EXPECT_TRUE(underTest.validate());
}

TEST_F(DepthControlTest, invalid_triag_minalt_zero) {
  auto underTest = buildFromString(R"( {
  "depth_mode": 3,
  "depth_min_altitude": 0.0,
  "depth_ceiling": 1.0,
  "depth_triangle_rate": 1.0,
  "depth_floor": 100.0
})");

  EXPECT_FALSE(underTest.validate());
}

TEST_F(DepthControlTest, invalid_triag_minalt_notset) {
  auto underTest = buildFromString(R"( {
  "depth_mode": 3,
  "depth_ceiling": 1.0,
  "depth_triangle_rate": 1.0,
  "depth_floor": 100.0
})");

  EXPECT_FALSE(underTest.validate());
}

TEST_F(DepthControlTest, invalid_triag_ceiling_zero) {
  auto underTest = buildFromString(R"( {
  "depth_mode": 3,
  "depth_min_altitude": 1.0,
  "depth_ceiling": 0.0,
  "depth_triangle_rate": 1.0,
  "depth_floor": 100.0
})");

  EXPECT_TRUE(underTest.validate());
}

TEST_F(DepthControlTest, invalid_triag_negceiling) {
  auto underTest = buildFromString(R"( {
  "depth_mode": 3,
  "depth_min_altitude": 1.0,
  "depth_ceiling": -1.0,
  "depth_triangle_rate": 1.0,
  "depth_floor": 100.0
})");

  EXPECT_FALSE(underTest.validate());
}

TEST_F(DepthControlTest, invalid_triag_ceiling_notset) {
  auto underTest = buildFromString(R"( {
  "depth_mode": 3,
  "depth_min_altitude": 1.0,
  "depth_triangle_rate": 1.0,
  "depth_floor": 100.0
})");

  EXPECT_FALSE(underTest.validate());
}

TEST_F(DepthControlTest, invalid_triag_norate) {
  auto underTest = buildFromString(R"( {
  "depth_mode": 3,
  "depth_min_altitude": 1.0,
  "depth_ceiling": 1.0,
  "depth_floor": 100.0
})");

  EXPECT_FALSE(underTest.validate());
}

TEST_F(DepthControlTest, invalid_triag_negfloor) {
  auto underTest = buildFromString(R"( {
  "depth_mode": 3,
  "depth_min_altitude": 1.0,
  "depth_ceiling": 1.0,
  "depth_triangle_rate": 1.0,
  "depth_floor": -1.0
})");

  EXPECT_FALSE(underTest.validate());
}

TEST_F(DepthControlTest, invalid_mode) {
  auto underTest = buildFromString(R"( {
  "depth_mode": -1,
  "depth_min_altitude": 1.0,
  "depth_ceiling": 1.0,
  "depth_triangle_rate": 1.0,
  "depth_floor": -1.0
})");

  EXPECT_FALSE(underTest.validate());
}