/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/2/20.
//

#include <gtest/gtest.h>

#include <ds_mxcore/test/ds_mock_compiler.h>

#include <ds_mx_stdprimitives/speed_control.h>

class SpeedControlTestTask : public ds_mx::MxPrimitiveTask {
 public:
  SpeedControlTestTask() : speed_control(params, "speed") { /* do nothing */ };

  // just make public, because test
  ds_mx_stdprimitives::SpeedControl speed_control;

  // these methods must be implemented because they're pure virtual
  bool validate() const override { return speed_control.validate(); }
  void getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay& display) override { /* do nothing */}
  void init_ros(ros::NodeHandle& nh) override { /* do nothing */ }

 protected:
  ds_mx::TaskReturnCode onTick(const ds_nav_msgs::NavState& state) override {
    // doesn't really matter what we do here
    return ds_mx::TaskReturnCode::RUNNING;
  }
};

class SpeedControlTest : public ::testing::Test {
  void SetUp() override {
    /* do nothing */
  }

 public:
  const ds_mx_stdprimitives::SpeedControl& buildFromString(const std::string& str) {
    // parse the JSON
    Json::Value config;
    Json::Reader reader;
    reader.parse(str, config);

    std::shared_ptr<ds_mx::MxMockCompiler> compiler = ds_mx::MxMockCompiler::create();
    test_task.init(config, compiler);

    return test_task.speed_control;
  }

  SpeedControlTestTask test_task;


};

TEST_F(SpeedControlTest, valid_openloop) {
  auto underTest = buildFromString( R"( {
  "speed_mode": 0,
  "speed_value": 1.0
})");

  EXPECT_TRUE(underTest.validate());
}

TEST_F(SpeedControlTest, valid_closedloop) {
  auto underTest = buildFromString( R"( {
  "speed_mode": 1,
  "speed_value": 1.0
})");

  EXPECT_TRUE(underTest.validate());
}

// there's no validation on speed values

TEST_F(SpeedControlTest, invalid_mode) {
  auto underTest = buildFromString( R"( {
  "speed_mode": -1,
  "speed_value": 1.0
})");

  EXPECT_FALSE(underTest.validate());
}
